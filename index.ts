/** @license 0BSD
  Copyright (C) 2024 8 Hobbies, LLC <hong@8hobbies.com>

  Permission to use, copy, modify, and/or distribute this software for anypurpose with or without
  fee is hereby granted.

  THE SOFTWARE IS PROVIDED "AS IS" AND THE AUTHOR DISCLAIMS ALL WARRANTIESWITH REGARD TO THIS
  SOFTWARE INCLUDING ALL IMPLIED WARRANTIES OFMERCHANTABILITY AND FITNESS. IN NO EVENT SHALL THE
  AUTHOR BE LIABLE FORANY SPECIAL, DIRECT, INDIRECT, OR CONSEQUENTIAL DAMAGES OR ANY
  DAMAGESWHATSOEVER RESULTING FROM LOSS OF USE, DATA OR PROFITS, WHETHER IN ANACTION OF CONTRACT,
  NEGLIGENCE OR OTHER TORTIOUS ACTION, ARISING OUT OFOR IN CONNECTION WITH THE USE OR PERFORMANCE OF
  THIS SOFTWARE.
 */
import globals from "globals";
import react from "eslint-plugin-react";
import reactHooks from "eslint-plugin-react-hooks";
import reactRefresh from "eslint-plugin-react-refresh";

const ignores = ["dist/*", "docs/*"] as const;

/** Generates recommended settings for react projects.
 *
 * @param tsconfigRootDir - Typically `import.meta.dirname` when called.
 * @param project - The project as in `languageOptions.parseOptions.project` in eslint typescript
 * configurations.
 * @example See `eslint.config.mjs`
 */
// eslint-disable-next-line @typescript-eslint/explicit-function-return-type
function recommended(
  tsconfigRootDir: string,
  project: true | string[] = ["tsconfig.json", "tsconfig.node.json"],
) {
  return [
    {
      ignores,
    },
    {
      files: ["**.ts", "**.tsx"],
      plugins: {
        react,
        "react-hooks": reactHooks,
        "react-refresh": reactRefresh,
      },
      languageOptions: {
        globals: {
          ...globals.browser,
        },
        parserOptions: {
          project,
          tsconfigRootDir,
          ecmaFeatures: {
            jsx: true,
          },
        },
      },
    },
  ] as const;
}

export default {
  recommended,
  ignores,
};
